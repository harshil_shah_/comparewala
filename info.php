<?php
require_once 'header.php';
require_once 'includes/functions.php';
$products = unserialize($_SESSION['products']);
$pid=$_GET['pid'];

$product = searchById($pid , $products);

$productId = $product->getProductId();
$title = $product->getProductTitle();
$productImage = $product->getImageUrl();
$productDescription = $product->getProductDescription;
$image = $product->getImage();
$price = $product->getPrice();
$url = $product->getUrl();
for($i = 1 ; $i <= count($url) ; $i++ ){
	
	if(strpos($url[$i], "flipkart") != false) {
		$tempNumber = strpos($url[$i], "affid");
		$tempUrl = substr($url[$i] , 0 , $tempNumber+6 );
		$url[$i] = $tempUrl.'harshilsh2';	
	}
	else if (strpos($url[$i], "amazon") != false) {
		$tempNumber = strpos($url[$i], "tag");
		$tempUrl = substr($url[$i] , 0 , $tempNumber+4 );
		$url[$i] = $tempUrl.'vardhinfoc-21';
	}
	else {

	}
}
?>
<div class="container" id="result">
<div class="panel shadow-z-2">
<div class="panel-body product-info-panel">
<a class="btn btn-default pull-left" href="./" title="Go Back" style="margin: 10px 10px 20px 10px;    padding-top: 10px;"><i class="fa fa-arrow-left fa-lg"></i></a>
	<h4><?php echo $title;?></h4>
	<div class="row ">
	<div class="col-md-3 product-img">
	<img src="<?php echo $productImage; ?>" >
	</div>
	<div class="col-md-9">
	<div class="row brands">
	
	<?php 
	for($i=1;$i <= count($url) ; $i++ ){ ?>
	<div class='col-md-4'>
	<img src="<?php echo $image[$i]; ?>" ><br/>
	<b><?php echo $price[$i]; ?></b><br/>
	<a href="<?php echo $url[$i]; ?>" target="_blank" class="btn btn-success btn-raised hvr-icon-forward">Buy Now&nbsp;&nbsp;</a>
	</div>

	<?php } 
	?>
	<!-- <div class="col-md-4">
	<img src="images/Flipkart.png"><br />
	<b><?php echo $currency."&nbsp;".$priceFlipkart;?></b><br />
	<a href="<?php echo $flipkartUrl;?>" target="_blank" class="btn btn-success btn-raised hvr-icon-forward">Buy Now&nbsp;&nbsp;</a>
	</div>
	<div class="col-md-4">
	<img src="images/Amazon.png" ><br />
	<b><?php echo $currency."&nbsp;".$priceAmazon;?></b><br />
	<a href="<?php echo $amazonUrl; ?>" target="_blank" class="btn btn-success btn-raised hvr-icon-forward">Buy Now&nbsp;&nbsp;</a>
	</div>
	<div class="col-md-4">
	<img src="images/Ebay.png" ><br />
	<b>N/A</b><br />
	<a href="" class="btn btn-success btn-raised hvr-icon-forward disabled">Buy Now&nbsp;&nbsp;</a>
	</div> -->
	</div>
	<div class="row description">
			<h5><b>Description</b></h5>
			<?php echo $productDescription;?>
	</div><!--row-->
	</div><!--col-md-9-->
	</div><!--row-->
	</div><!-- panel-body -->
	</div><!-- panel -->
</div><!-- container -->
<?php require_once 'footer.php';?>