<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Compare Wala</title>
    <link rel="icon" href="images/comparewala.png" type="image/png" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.3.0/css/roboto.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.3.0/css/material.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.3.0/css/ripples.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/animatecss/3.5.1/animate.min.css">
    <link rel="stylesheet" href="css/main.css"/>


  
    
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top shadow-z-1" role="navigation">
<div class="container">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <div class="navbar-brand" href="#" style="font-weight:bold;">
    <div class="brand-header">
    <img src="images/comparewala.png" class="brand-image">
    <a href="./"><span style="color:#43781e;">Compare</span><span style="color:#f30909;">Wala</span></a>
    </div>
      <div class="col-sm-3 col-md-6 hidden-sm hidden-xs" style="margin-top: 10px;">
     
        <div id="custom-search-input">
                <div class="input-group col-md-12 ">
                    <input type="text" class="form-control input-lg search"  id="searchid" name="keyword" placeholder="Search" />
                    <div class="suggesion-result" style="display:none;"></div>

                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button" id="search-btn">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
        </div>
    </div>
    </div>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <div class="header-list">
    <ul class="nav navbar-nav">
      <li><a class="hvr-shutter-out-horizontal" href="#" id="list"><i class="fa fa-mobile"></i>&nbsp;Mobiles</a></li>
      <li><a class="hvr-shutter-out-horizontal" href="#" id="list"><i class="fa fa-television"></i>&nbsp;Tvs</a></li>
      <li><a class="hvr-shutter-out-horizontal" href="#" id="list"><i class="fa fa-tablet"></i>&nbsp;Tablets</a></li>
      <li><a class="hvr-shutter-out-horizontal" href="#" id="list"><i class="fa fa-camera"></i>&nbsp;Cameras</a></li>
      <li><a class="hvr-shutter-out-horizontal" href="#" id="list"><i class="fa fa-laptop"></i>&nbsp;Laptops</a></li>
      <!--<li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
        <ul class="dropdown-menu shadow-z-1">
          <li><a href="#"  id="list">Mobile</a></li>
          <li><a href="#"  id="list">TV</a></li>
          <li><a href="#"  id="list">Something else here</a></li>
          <li class="divider"></li>
          <li><a href="#">Separated link</a></li>
          <li class="divider"></li>
          <li><a href="#">One more separated link</a></li>
        </ul>
      </li>-->
    </ul> 
    </div>
   </div>
   </div>
</nav>
<div id="loaderDiv" style="display:none;"><div class="loader">Loading...</div></div>
  <div class=" visible-sm visible-xs" style="padding: 50px;">
     
        <div id="custom-search-input visible-sm visible-xs">
                <div class="input-group col-md-12 ">
                    <input type="text" class="form-control input-lg search"  id="searchid" name="keyword" placeholder="Search" />
                    <div class="suggesion-result" style="display:none;"></div>

                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button" id="search-btn">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
        </div>
    </div>