To open and run the project you will need WAMP or XAMPP server
to download WAMP : http://www.wampserver.com/en/
to download XAMPP : https://www.apachefriends.org/download.html

if using WAMP : 
	paste the code in www directory (By default you will find www directory in C:\wamp\)
	start the server
	open web browser
	go to localhost/127.0.0.1
	open the folder
	its'done
if using XAMPP :
	paste the code in XAMPP/htdocs/any_new_folder 
	start the server
	open web browser
	go to localhost/127.0.0.1
	open the folder
	its'done


Features :
	1. Compare prices from top e-stores
	2. Responsive design
	3. Material design
