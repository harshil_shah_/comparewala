<!-- jQuery -->
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<!-- bootstrap.min.js -->
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!-- js for ripple effect -->
<script type="text/javascript" src="js/ripples.min.js"></script>
<!-- material design -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.3.0/js/material.min.js"></script>
<!-- initializing material design -->
<script type="text/javascript">
    $(document).ready(function () {         
        $.material.init();
    });
</script>
<!-- wow js for animations-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script type="text/javascript">
new WOW().init();
</script>
    
<script type="text/javascript">
//for showing result of link on header
$("a#list").click(function() 
{
  var searchid = $(this).text();
  var dataString = 'keyword='+ searchid;
  if(searchid!='')
  {
      $.ajax({
      type: "POST",
      url: "search.php",
      beforeSend: function(){
                         $("#loaderDiv").show();
                         $("#result").hide();
                         //$("li.dropdown>a").attr("aria-expanded","false");
                         //$("li.dropdown").removeClass("open");
                     },
      data: dataString,
      cache: false,
      success: function(html)
      {
      $("#loaderDiv").hide();
      $("div#result").html(html).show();
      }
      });
  }
  else
  {
    //$("#result").hide();
  }
return false; 
});

//to search on enter key press
$('#searchid').keypress(function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
    $('#search-btn').click();
    return false;  
  }
});

//to search when search button clicked
$(function(){
  $("#search-btn").click(function() 
  { 
    $.get("set.php");
    var searchid = $(".search").val();
    var dataString = 'keyword='+ searchid;
    if(searchid!='')
    {
      $.ajax({
      type: "POST",
      url: "search.php",
      beforeSend: function(){
                         $("#loaderDiv").show();
                         $("#result").hide();
                     },
      data: dataString,
      cache: false,
      success: function(html)
      {
      $("#loaderDiv").hide();
      $("#result").html(html).show();
      }
      });
    }
    else
    {
      $("#result").hide();
    }
  return false;    
  });

  jQuery("#result").on("click",function(e){ 
      var $clicked = $(e.target);
      var $name = $clicked.find('.name').html();
      var decoded = $("<div/>").html($name).text();
      $('#searchid').val(decoded);
  });
  /*jQuery(document).live("click", function(e) { 
      var $clicked = $(e.target);
      if (! $clicked.hasClass("search")){
      jQuery("#result").fadeOut(); 
      }
  });*/
  $('#searchid').click(function(){
      jQuery("#result").fadeIn();
  });
});
</script>

<script type="text/javascript">
//to shrink navigation bar when page is scrolled
var smallWidth=80;
var largeWidth=135;
$(window).resize(function() {

  if ($(this).width() > 1199) {
    //for large screen device
    smallWidth=80;
    largeWidth=135;
  }
  else if($(this).width() < 1200  && $(this).width()>766){
    // for tablet or ipad screen size
    smallWidth=80-5;
    largeWidth=135-15;
  }
  else if($(this).width() < 767){
    //for mobile screen 
      smallWidth=largeWidth=80;
  }
 });

$(document).scroll(function(){
  if ($(this).scrollTop()>largeWidth){
      // animate fixed div to small size:
      $('.navbar').stop().animate({ height: smallWidth},100);
      $('.header-list').hide();
  } else {
      //  animate fixed div to original size
      $('.navbar').stop().animate({ height: largeWidth},100);
       $('.header-list').show();
  }
});       
$(document).ready(function() {
   $(window).resize();
});
</script>
</body>
</html>