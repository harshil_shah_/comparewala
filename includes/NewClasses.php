<?php 
	
	$index = 0;
	class Product {
		protected $productPrice;
		#protected $prceAmazon;
		#protected $priceEbay;
		protected $productMinPrice;
		protected $productId;
		protected $productTitle;
		protected $imageUrl;
		protected $productUrl;
		protected $productDescription;
		protected $productBrand;
		protected $productDetailUrl;
		protected $productImage;
		

		function __construct($productTitle , $productMinPrice,  $imageUrl , $productDetailUrl) {
			global $index;
			$this->productTitle = $productTitle;
			$this->productMinPrice = $productMinPrice;
			$this->imageUrl = $imageUrl;
			$this->productDetailUrl = $productDetailUrl;
			$this->productId = $index++;
		}


		function getProductPrice(){return $this->productPrice;}
		function getMinPrice(){return $this->productMinPrice;}
		function getProductId(){return $this->productId;}
		function getProductTitle(){return $this->productTitle;}
		function getImageUrl(){return $this->imageUrl;}
		function getProductUrl(){return $this->productUrl;}
		function getProductDescription(){return $this->productDescription;}
		function getProductBrand(){return $this->productBrand;}
		function getProductDetailUrl(){ return $this->productDetailUrl;}
		function setPrice($price){$this->productPrice = $price;}
		function setImage($image){$this->productImage = $image;}
		function setUrl($url){$this->productUrl = $url;}
		function getPrice(){return $this->productPrice;}
		function getImage(){return $this->productImage;}
		function getUrl(){return $this->productUrl;}
		function setDescription($feature){ 
			$x = '<ul>'.$feature.'</ul>';
			$this->getProductDescription = $x;
		 }
	}
	
?>