<?php 
	class Product {
		private $priceFlipkart;
		#private $prceAmazon;
		#private $priceEbay;
		private $minPrize;
		private $productId;
		private $productTitle;
		private $imageUrl;
		private $productUrl;
		private $productDescription;
		function __construct($product){
			$this->priceFlipkart = $product["productAttributes"]["sellingPrice"]["amount"];
			$this->minPrize = $this->priceFlipkart;
			$this->productId = $product["productIdentifier"]["productId"];
			$this->productTitle = $product["productAttributes"]["title"];
			$this->imageUrl = $product["productAttributes"]["imageUrls"]["275x275"];
			$this->productUrl = $product["productAttributes"]["productUrl"];
			$this->productDescription = $product["productAttributes"]["productDescription"];
		}
		function getPriceFlipkart() {return $this->priceFlipkart;}
		function getMinPrice() {return $this->minPrize;}
		function getProductId() {return $this->productId;}
		function getProductTitle() {return $this->productTitle;}
		function getImageUrl() {return $this->imageUrl;}
		function getProductUrl() {return $this->productUrl;}
		function getProductDescription() {return $this->productDescription;}
	}

?>