<?php 
require_once "NewClasses.php";
function getProductByKeyword($keyword)
{
    $keyword = str_replace(" ", "-", $keyword);
    $url = "http://www.compareraja.in/search?q=".$keyword."&c=all";
    $ch = curl_init();
    //$proxy = "192.168.100.100:808";
    curl_setopt($ch, CURLOPT_URL , $url );
    //curl_setopt($ch, CURLOPT_PROXY, $proxy);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE );
    $result = curl_exec($ch);
    $result2 = explode('<ul class="mrgn-top">' , $result);
    $result3 = explode('</ul>' , $result2[1]);
    $product = explode('</li>' , $result3[0]);
    $products = array();
    $k = 0;
    for($i = 0 ; $i < count($product) -1 ; $i++ ) {
        $titleStart =  strpos($product[$i], "<p>"); 
        $titleStop = strpos($product[$i] , "</p>" , $titleStart);
        $length = $titleStop - $titleStart + 4;
        $title = substr($product[$i], $titleStart + 4 , $length - 9);

        $priceStart = strpos($product[$i] , "</span>");
        $priceEnd = strpos($product[$i] , "<span " , $priceStart  );
        $length = $priceEnd - $priceStart + 4;
        $price = substr($product[$i], $priceStart + 7 , $length - 11);

        $imageStart = strpos($product[$i] , "src=");
        $imageEnd = strpos($product[$i] , "alt" , $imageStart + 6);
        $length = $imageEnd - $imageStart + 5;
        $image = substr($product[$i] , $imageStart+5 , $length-12);

        $urlStart = strpos($product[$i] , "href=");
        $urlEnd = strpos($product[$i], '">' , $urlStart);
        $length = $urlEnd - $urlStart;
        $url = substr($product[$i] , $urlStart + 6 , $length-6);
        if($url != 'http://www.compareraja.in/-price.html') {
            $products[$k] = new Product($title,$price,$image ,$url);
            $k++;
        }
    }

    for ($i= 0 ; $i < count($products) ; $i++) {
        $data = getData($products[$i]->getProductDetailUrl());
        $products[$i]->setPrice($data['price']);
        $products[$i]->setUrl($data['url']);
        $products[$i]->setImage($data['image']);
        $products[$i]->setDescription($data['feature']);
    }

    if( isset($_SESSION['sproducts'] ))
        unset($_SESSION['products']);
    $_SESSION['products'] = serialize($products);
    $_SESSION['new'] = 1;
    return $products; 
}

function searchById($pid , $products){
    foreach ($products as $product  ){
        if ( $pid == $product->getProductId()) {
            return $product;
        }
    }
}

function getData($productUrl) {
    $url = $productUrl;
    $ch = curl_init();
    //$proxy = "192.168.100.100:808";
    curl_setopt($ch, CURLOPT_URL , $url );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE );
    //curl_setopt($ch, CURLOPT_PROXY, $proxy);
    $result = curl_exec($ch); 

    $result1 = explode('<section class="nexmob-top">', $result);
    $result1 = explode('</section>', $result1[1]);
    $result1 = $result1[0];

    $divPos = strpos($result1, 'ProductDetails1_FeatureControl1_divFeatures');
    $inputPos = strpos($result1 , 'class="nexmob-lst-rht"');
    $length = $inputPos - $divPos;
    $featureTemp = substr($result1, $divPos , $length);
    $featureTemp = explode('<ul class="nexmob-lst-nw">', $featureTemp);
    $featureTemp = $featureTemp[1];

    $divPos = strpos($result1, '<div class="mer-box1">');
    $inputPos = strpos($result1 , 'id="hdncount"');
    $length = $inputPos - $divPos;
    $result1 = substr($result1, $divPos , $length);

    $result1 = explode('<div class="mer-box1">',$result1);

      $price = array();
      $image = array();
      $url = array();
      $final = array();
      for($i = 1 ; $i < count($result1) ; $i++ ) {

    $resultTemp = $result1[$i];
    $resultTemp = explode('target="_blank" class="prd-link"',$resultTemp);

    //To Get LinkUrl
    $linkurl=explode('onclick="exitoblnew(', $resultTemp[0]);
    $linkurl=explode(',', $linkurl[1]);
    $linkurl=$linkurl[0];
    $linkurl = substr($linkurl, 1);
    $linkurl = substr($linkurl , 0 , strlen($linkurl) - 1);

    //TO get Image Url
    $imageTemp = explode('</figure>',$resultTemp[1]);
    $imageTemp=explode('<img src=', $imageTemp[0]);
    $imageTemp=explode('width', $imageTemp[1]);
    $imageTemp=$imageTemp[0];
    $imageTemp = substr($imageTemp, 1);
    $imageTemp = substr($imageTemp , 0 , strlen($imageTemp) - 2);

    //TO get Product Price
    $productPrice=explode('<p class="mer-text">', $resultTemp[1]);
    $productPrice=explode('</p>', $productPrice[1]);
    $productPrice=$productPrice[0];

    $image[$i] = $imageTemp;
    $price[$i] = $productPrice;
    $url[$i] = $linkurl;
       
}

$final['image'] = $image;
$final['price'] = $price;
$final['url'] = $url;
$final['feature'] = $featureTemp;
 
return $final;
}
?>