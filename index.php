<?php require_once 'header.php';
require_once 'includes/functions.php';
$keyword = "mobile";
if(isset($_SESSION['products']) && $_SESSION['new'] == 1) {
$products=unserialize($_SESSION['products']);
}
else {
$products=getProductByKeyword($keyword);
};
?>
<div class="container" id="result">

<?php
$count=0;
$i=0;
foreach ($products as $product) {
	$title=$product->getProductTitle();
	$productImage = $product->getImageUrl();
	$sellingPrice=$product->getMinPrice();
	$currency="Rs.";
	$productUrl = $product->getProductUrl();
	//$productInfo="https://affiliate-api.flipkart.net/affiliate/product/json?id=".$product->getProductId();
	$productId = $product->getProductId();
?>

<?php if($count%4==0)
		echo '<div class="row">';
?>

<div class="col-md-3">
	<div class="panel shadow-z-1 product wow bounceInUp" data-wow-duration="1s" style="max-hieght:100px;">
		<div class="panel-thumbnail product-image"><img src="<?php echo $productImage;?>" style="max-height:200px;max-width:200px;"></div>
		
		<div class="panel-footer product-details">
		<h4><a href="./info.php?pid=<?php echo $productId;?>"><?php echo $title;?></a></h4>
		<!-- <a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Amazon : Rs.1000<br>Flipkart : Rs.1000"  class="red-tooltip">Selling Price: <b>
		<?php echo $currency."&nbsp;".$sellingPrice;?></b></a> -->
		<a>Selling Price: <b>
		<?php 
		if($sellingPrice!="")
			echo $sellingPrice;
		else
			echo "Not Available";
		?></b>
		</a>
		</div>
	</div>
</div>
<?php if(($i+1)%4==0)
		echo '</div>';
?>
<?php
$i++;
$count++;
}?>
</div>
<?php require_once 'footer.php';?>